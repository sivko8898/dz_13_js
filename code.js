'user strict'
/*
    1.Метод setInterval має такий самий синтаксис як setTimeout:
  let timerId = setInterval(func|code, [delay], [arg1], [arg2], ...);
  Усі аргументи мають таке саме значення. Але відмінність цього 
  методу від setTimeout полягає в тому, що функція запускається 
  не один раз, а періодично через зазначений інтервал часу.

    2.Так спрацює митьєво, бо немає ніякої затримки.

    3. Для того щоб рано чи пізно зупинити функцію setInterval.
*/

let slides = document.querySelectorAll('#images-wrapper > .image-to-show');

let currentImg = 0;

function nextImg() {
    slides[currentImg].classList = 'image-to-show';
    currentImg = (currentImg + 1)%slides.length;
    slides[currentImg].classList = 'image-to-show active';
}

let imgInterval = setInterval(nextImg, 3000);

let playing = true;
let pauseButton = document.getElementById('pause');
let playButton = document.getElementById('play')
function pauseSlideshow() {
    pauseButton.innerHTML = 'Припинити';
    playing = false;
    clearInterval(imgInterval);
}
function playSlideshow() {
    playButton.innerHTML = 'Відновити показ';
    playing = true;
    imgInterval = setInterval(nextImg,3000);
}
pauseButton.onclick = function() {
    pauseSlideshow();
};

playButton.onclick = function() {
  playSlideshow();
};
